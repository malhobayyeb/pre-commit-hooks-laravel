################################################################################
#
# Locate the command from a list of options
#
################################################################################

# Final location of the executable that we found by searching
exec_command=""

if [ -f "$vendor_command" ]; then
    exec_command=$vendor_command
elif hash $global_command 2>/dev/null; then
    exec_command=$global_command
else
    echo -e "${bldred}No valid ${title} found!${txtrst}"
    echo "Please have one available as one of the following:"
    echo " * $vendor_command"
    echo " * $global_command"
    exit 1
fi