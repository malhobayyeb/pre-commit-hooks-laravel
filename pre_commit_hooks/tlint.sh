#! /bin/bash

# Laravel and PHP Code Linter
#
# Exit 0 if no errors found
# Exit 1 if errors were found
#
# Requires
# - None
#
# Arguments
# - None

# Plugin title
title="Laravel and PHP Code Linter"

# Possible command names of this tool
vendor_command="vendor/bin/tlint"
global_command="tlint"

# Print a welcome and locate the exec for this tool
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/helpers/colors.sh
source $DIR/helpers/formatters.sh
source $DIR/helpers/welcome.sh
source $DIR/helpers/locate.sh

tlint_command="${exec_command}"

echo -e "${bldwht}Running command ${txtgrn} ${tlint_command}"

command_result=`eval $tlint_command`

if [[ $command_result != "LGTM!" ]]
then
    hr
    echo -en "${bldmag}Errors detected by TLint ... ${txtrst} \n"
    hr
    echo "$command_result"
    exit 1
fi
exit 0
